% Project name
% Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
% Date: 09.04.2017

\documentclass[12pt, a4paper]{article}

%\usepackage[left=2cm, text={17cm, 24cm}, top=3cm]{geometry}
\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}


\setlength{\hoffset}{-1.0cm}
\setlength{\voffset}{-1.5cm}
\setlength{\textheight}{23.0cm}
\setlength{\textwidth}{15.5cm}

%---rm---------------
\renewcommand{\rmdefault}{lmr}
%---sf---------------
\renewcommand{\sfdefault}{qhv}
%---tt------------
\renewcommand{\ttdefault}{lmtt}


\bibliographystyle{czplain}

\begin{document}

\begin{titlepage}
\begin{center}
\Huge
\textsc{Vysoké učení technické v Brně \\
\huge
Fakulta informačních technologií}\\
\vspace{\stretch{0.382}}
\LARGE
Fyzikální optika\\
\Huge
Spektrální analýza světelného svazku. Barevné souřadnice.
\vspace{\stretch{0.618}}
\end{center}
{\Large \today \hfill Matúš Motlík}

\end{titlepage}

\tableofcontents
\newpage


\section{Úvod}
Elektromagnetické žiarenie je priečne postupné vlnenie elektromagnetického poľa. Podľa vlnovej dĺžky rozlišujeme toto žiarenie na niekoľko druhov -- viditeľná čast spektra v rozmedzí približne od 390 do 760 nm, ďalej infračervené žiarenie, mikrovlny, rádiovlny, ultra-fialové, Röntgenové a gamma žiarenie, viď obrázok \ref{fig:spectrum}.

Spektrálna analýza študuje spektrum svetla, teda výskyt jednotlivých vlnových dĺžok v spektre. V rámci tejto práce sa budeme zaoberať spektrálnou analýzou viditeľnej časti spektra, bude vysvetlený jej princíp a použitie. Ďalej bude popísaný princíp vzniku emisných a absorpčných čiar v spektre a postup získavania informácií o hviezdach z ich svetelného spektra. V poslednej časti bude stručne popísaná implementácia demo aplikácie.

\begin{figure}[!ht]
  \centering
  \includegraphics[scale=0.4]{figs/SL_EMspectrum.jpg}
  \caption{Delenie elektromagnetického spektra, prevzaté z~\cite{spectrumFig}}
  \label{fig:spectrum}
\end{figure}
 
\section{Spektrálna analýza}
V minulom storočí bolo experimentálne dokázané, že svetlo má tzv. duálny charakter. V mnohých prípadoch sa chová ako častica, inokedy má vlnovú povahu. Častice svetla voláme fotóny \cite{blairBasics:web}. Tie sa od seba líšia svojou energiou. Pri vlnovom modeli svetla rozlišujeme jednotlivé druhy podľa vlnovej dĺžky. Tento duálny charakter svetla nám umožňuje definovať vzájomný vzťah pre energiu fotónu a vlnovú dĺžku svetla $$E = h*c/ \lambda$$ kde $h$ je plancová konštanta a $c$ je rýchlosť svetla \cite{blairBasics:web}.

Vlastnosťou častíc (atómov, molekúl) je, že dokážu absorbovať energiu, teda jednotlivé fotóny. Toto sa využíva v rade oblastí, od medicíny, techniky až po astronómiu. Výsledkom je, že pri prechode určitou látkou, je časť svetla absorbovaná, čo sa vo výsledku prejaví v jeho spektre ako tmavšie spektrálne čiary. Na základe toho rozlišujeme spojité, absorpčné a emisné spektrum, viď obrázok \ref{fig:spectrumTypes}. 

\begin{figure}[!ht]
  \centering
  \includegraphics[scale=0.8]{figs/sodium.jpg}
  \caption{Príklad spojitého spektra (hore), emisného (stred) a absorpčného (dole) spektra pre prvok sodík, prevzaté z \cite{sodiumSpectra}}
  \label{fig:spectrumTypes}
\end{figure}

Spojité spektrum je súvislé, získame ho napríklad rozkladom bieleho spektra cez optický hranol. Tento experiment ako prvý uskutočnil I. Newton \cite{lightAndMatter}. Emisné spektrum zvýrazňuje vlnové dĺžky, ktoré sú vyžiarené určitým materiálom, napríklad horúcim plynom \cite{blairBasics:web}. Absorpčné spektrum naopak obsahuje tmavšie pruhy, ktoré sú zvyčajne spôsobené pohltením svetla o daných vlnových dĺžkach nejakou látkou. Vzniká napríklad pri prechode svetla so spojitým spektrom chladnejším plynom, ktoré absorbuje časť tejto energie.

\subsection{Vznik absorpčných a emisných čiar}
Predstavme si model jedného atómu vodíka. Vodík sa skladá z jedného protónu a elektrónu. Uvažujme tzv. Bohrov model atómu vodíka, ktorý je naznačený na obr. \ref{fig:hydrogenAtom}, kde sa v jeho strede nachádza protón a vo vonkajších hladinách sa nachádza elektrón. Tento model nie je veľmi presný, dokáže popisovať chovanie atómov, ktoré majú iba jeden valenčný elektrón, napríklad $H$, $He^+$ a iné \cite{lightAndMatter}. Je však jednoduchý pre predstavu a na pochopenie princípu chovania elektrónu. 

Bohrov model definuje, že elektrón sa môže nachádzať iba v presne špecifikovaných energetických hladinách. Pri prechode z jednej hladiny na druhú je vyžiarený, alebo pohltený práve jeden fotón, ktorý má energiu rovnajúcu sa rozdielu týchto energetických hladín, viď obrázok \ref{fig:hydrogenAtom}. V energeticky neutrálnom stave sa elektrón nachádza na prvej energetickej vrstve. V prípade, že elektrónu dodáme dostatočnú energiu, napríklad ožiarime tento atóm svetelným paprskom, dodaná energia spôsobí, že elektrón "poskočí" na vyššiu energetickú hladinu, tzn. elektrón absorboval tento fotón \cite{blairBasics:web}. Tento fotón s danou vlnovou dĺžkou preto bude "chýbať" v spojitom spektre svetla, vyžiareného napríklad z hviezdy. To sa prejaví ako absorpčná spektrálna čiara. 

Atóm vodíku sa teraz nachádza v tzv. excitovanom stave. Tento stav však nie je prirodzený a preto po určitom čase sa elektrón vráti na pôvodnú energetickú hladinu. Pri tomto prechode uvoľní absorbovanú energiu vo forme fotónu. Takéto emitovania fotónov o určitých vlnových dĺžkach je možné pozorovať v emisnom spektre horúcich plynov \cite{blairBasics:web}. Tento proces sa môže opakovať viac krát. V prípade, že je elektrónom absorbovaná dostatočná energia, môže elektrón prejsť na omnoho vyššiu energetickú hladinu a následne emitovať vo forme viacerých fotónov absorbovanú energiu, až kým sa nedostane opäť do základného stavu. Ak však dodáme elektrónu príliš veľkú energiu, atóm bude ionizovaný \cite{blairBasics:web}.

Jednotlivé energetické hladiny, v ktorých sa môže nachádzať elektrón vodíka, sú znázornené na obrázku \ref{fig:hydrogenAtom}. Tieto "povolené" energetické prechody elektrónu definujú celú škálu absorbovaných alebo emitovaných fotónov o presne danej vlnovej dĺžke. Tieto absorbované/emitované fotóny je možné pozorovať ako absorpčné/emisné čiary v spektre. Tento vzor spektrálnych čiar je jedinečný pre vodík. Podľa toho, v ktorej časti spektra sa tieto emisné čiary nachádzajú, delíme ich na niekoľko sérií (Lyman, Balmer, Paschen, a iné) \cite{transitions}. Z toho ako prvé boli pozorované spektrálne čiary v Balmer sérií, keďže sa nachádzajú vo viditeľnej časti spektra.

Všetky prvky však majú jedinečné emisné spektrum. Vďaka tejto vlastnosti môžeme zistiť zloženie látok pri spektrálnej analýze, alebo napríklad pozorovaním spektra slnka, môžeme určiť zloženie jeho vonkajšieho plášťa. Príklady emisných spektier pre niektoré prvky sú znázornené na obrázku \ref{fig:emissionSpectraOfElements}. 


\begin{figure}[!ht]
  \centering
  \includegraphics[scale=0.7]{figs/Htransitions.png}
  \caption{Bohrov model atómu vodíka s vyznačenými energetickými hladinami elektrónu. Medzi týmito hladinami sú vyznačené výsledné vlnové dĺžky fotónu vyžiareného pri prechode z jednej hladiny na druhú.}
  \label{fig:hydrogenAtom}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[scale=0.7]{figs/AAAKKHX0.jpg}
  \caption{Príklad emisiných čiar niektorých prvkov. Prevzaté z \cite{lightAndMatter}}
  \label{fig:emissionSpectraOfElements}
\end{figure}



\subsection{Spektrum hviezd}
Jadro hviezdy vyžaruje svetlo, ktoré má spojité spektrum. To, že pozorujeme tmavé absorpčné pruhy v tomto spektre je spôsobené tým, že toto spojité svetlo prechádza cez chladnejší vonkajší plášť hviezdy. V tomto plášti sa nachádzajú plyny, ktoré náhodne absorbujú a emitujú fotóny o špecifických vlnových dĺžkach. Ako bolo popísané vyššie, každý chemický prvok má jedinečné emisné spektrum. Pozorovaním vzoru týchto spektrálnych čiar môžeme určiť existenciu tohto prvku vo vonkajšom plášti hviezdy \cite{lightAndMatter}.

Zo spektra hviezd dokážeme dnes získať veľké množstvo informácií. Napríklad analýzou viditeľnej časti spektra dokážeme zistiť jej chemické zloženie. To zistíme porovnávaním jeho spektrálnych čiar so známymi spektrálnymi čiarami jednotlivých prvkov \cite{lightAndMatter}. Teplotu hviezdy určíme pomocou Wienovho zákona $$\lambda_max T = b$$ kde $b$ je Wienova konštanta, $T$ je teplota a $\lambda_max$ je vlnová dĺžka maxima spektrálnej hustoty vyžarovania \cite{lightAndMatter}. Z teploty a svetelnosti hviezdy je možné určiť jej približnú veľkosť. Podľa veľkosti potom delíme hviezdy do jednotlivých kategórií od hviezdnych obrov po trpaslíky \cite{spectroscopy}. Pohyb hviezd dokážeme zistiť porovnávaním pozorovaných spektrálnych čiar s očakávanými čiarami. Tento posun je zapríčinený Dopplerovým efektom. V prípade, že sú spektrálne čiary posunuté do červenej časti spektra, hviezda sa od Zeme vzďaľuje \cite{doppler}. Tento posun je znázornený na obrázku \ref{fig:Doppler}.
	


	
\begin{figure}[!ht]
  \centering
  \includegraphics[width=10cm, height=5cm]{figs/doppler.jpg}
  \caption{Posun spektrálnych čiar spôsobený Dopplerovým efektom. Neposunuté spektrálne čiary (hore). Posunuté do červenej časti spektra, tzn. objekt sa vzďaľuje od pozorovateľa (stred). Posunuté do fialovej časti spektra, tzn. objekt sa vzďaľuje (dole). Prevzaté z \cite{doppler}}
  \label{fig:Doppler}
\end{figure}


\newpage

\section{Implementácia}
V praktickej časti tohto projektu bola vytvorená demo aplikácia pre demonštrovanie vzniku emisných čiar atómu vodíka. Užívateľovi sa zobrazuje tzv. Bohrov model atómu vodíka, kde je znázornený model protónu, elektrónu a jednotlivé energetické hladiny. Vpravo od tejto plochy sa nachádza panel so záznamom udalostí. Užívateľ môže na stupnici interaktívne nastaviť energiu fotónu, ktorým následne ostreľuje tento model atómu vodíka. Nad touto stupnicou sa zobrazuje prepočet energie fotónu na jeho vlnovú dĺžku. V prípade, že užívateľ nastaví dostatočnú energiu vystreleného svetelného zväzku, elektrón túto energiu absorbuje a presunie sa na vyššiu energetickú hladinu. Po určitom čase sa elektrón vráti do základného stavu a uvoľní absorbovaný fotón. Všetky tieto akcie sú postupne zaznamenávané v pravom paneli. 

Táto aplikácia bola implementovaná pomocou webových technológií, takže pre spustenie je potrebné mať aktuálny webový prehliadač. Ako implementačný jazyk bol použitý \texttt{JavaScript}, s knižnicou \texttt{jQuery} a externými knižnicami \texttt{PixiJS},  \texttt{Charm}, použitými pre vytvorenie animácií a \texttt{jQuery UI Slider Pips} pre vytvorenie posuvníka. 

\section{Záver}
V tejto práci bola stručne popísaný princíp spektrálnej analýzy a jej využitie.	Podrobnejšie je popísaný princíp vzniku spektrálnych čiar na Bohrovom modele atómu vodíka. Na základe tohto modelu mola implementovaná demo aplikácia, ktorá interaktívnym spôsobom umožňuje užívateľovi bližšie pochopiť daný problém. Následne bolo stručne popísane využitie spektrálnej analýzy v astronómií a popísané informácie, ktoré je možné získať z analýzy spektra hviezdneho žiarenia.


\clearpage
\newpage
\bibliography{literatura}

\end{document}